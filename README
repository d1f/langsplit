NAME
    langsplit - split text file by language sections.

DESCRIPTION
    The langsplit is an utility to split a plain (or ReSTructured) text file
    by human language sections.

    The language sections are separated by section header lines:

        .. lang=LANGUAGE

    ".LANGUAGE" is used as a suffix of the output file name.

    Especial LANGUAGE "common" designates sections that outputs to all of
    language files. First headerless section is "common".

    This utility is intended to use with multi-language documents in
    ReSTructured text format, at most.

    Maintenance of a language translation is easy if small sections of
    different languages are placed near of each other.

SYNOPSIS
    langsplit input [output-base]

OPTIONS
    input
        Input file name or - for stdin.

    output-base
        Output file names base. Optional. The input name is used if
        output-base is not provided.

AUTHOR
    Dmitry Fedorov <dm.fedorov@gmail.com>

COPYRIGHT
    Copyright (C) 2012 Dmitry Fedorov <dm.fedorov@gmail.com>

LICENSE
    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

DISCLAIMER
    The author disclaims any responsibility for any mangling of your system
    etc, that this script may cause.

